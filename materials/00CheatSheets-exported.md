Comandos útiles en línea de comandos
====================================

`pwd`
-----

Imprime el directorio en el que estás actualmente

    pwd

    ## /home/skalas/github/aacd/sesion01

`ls`
----

Comando que lista los archivos del directorio donde estás.

    ls

    ## 00CheatSheets-exported.html
    ## 00CheatSheets.Rmd
    ## 01IntroR-exported.html
    ## 01IntroR-exported.md
    ## 01IntroR.Rmd
    ## 02Install-exported.html
    ## 02Install-exported.md
    ## 02Install.Rmd
    ## 03Git-exported.html
    ## 03Git-exported.md
    ## 03Git.Rmd
    ## 04BasicR-exported.html
    ## 04BasicR.Rmd
    ## 05BasicR2-exported.html
    ## 05BasicR2.Rmd
    ## img
    ## outline.org

`mkdir`
-------

Make directory, crea un directorio.

    mkdir -p directorioPrueba

Este comando no regresa ningún resultado, para verificar que se hizo lo
que pedimos, usamos el comando `ls`

    ls

    ## 00CheatSheets-exported.html
    ## 00CheatSheets.Rmd
    ## 01IntroR-exported.html
    ## 01IntroR-exported.md
    ## 01IntroR.Rmd
    ## 02Install-exported.html
    ## 02Install-exported.md
    ## 02Install.Rmd
    ## 03Git-exported.html
    ## 03Git-exported.md
    ## 03Git.Rmd
    ## 04BasicR-exported.html
    ## 04BasicR.Rmd
    ## 05BasicR2-exported.html
    ## 05BasicR2.Rmd
    ## directorioPrueba
    ## img
    ## outline.org

`cd`
----

Change directory, te mueve de directorio. Este comando tampoco regresa
ningún resultado, de nuevo, para verificar que cambiamos de directorio
podemos usar `pwd`.

    cd directorioPrueba
    pwd

    ## /home/skalas/github/aacd/sesion01/directorioPrueba

El directorio inmediatamente superior al directorio donde estamos en
bash se denota como `..`. Entonces para subir un directorio escribimos:

    cd ..

Y de nuevo no tenemos resultado, simplemente debemos volver a escribir
`pwd`, o listamos los archivos del directorio donde quedamos :).

    pwd

    ## /home/skalas/github/aacd/sesion01

    ls

    ## 00CheatSheets-exported.html
    ## 00CheatSheets.Rmd
    ## 01IntroR-exported.html
    ## 01IntroR-exported.md
    ## 01IntroR.Rmd
    ## 02Install-exported.html
    ## 02Install-exported.md
    ## 02Install.Rmd
    ## 03Git-exported.html
    ## 03Git-exported.md
    ## 03Git.Rmd
    ## 04BasicR-exported.html
    ## 04BasicR.Rmd
    ## 05BasicR2-exported.html
    ## 05BasicR2.Rmd
    ## directorioPrueba
    ## img
    ## outline.org

`touch`
-------

Es un comando para crear un archivo vacío Entramos al directorio y
creamos un archivo prueba.txt, y checamos que de hecho se creara

    cd directorioPrueba
    touch prueba.txt
    ls

    ## prueba.txt

Efectivamente se creó.

`echo`
------

Comando para imprimir en pantalla lo que se le mande

    echo "Hola! Buenos días!"

    ## Hola! Buenos días!

`>`
---

este es un operador que le llamamos `pipe` Esto hace que lo que salga
del lado izquierdo, se imprima al lado derecho.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    echo "Esta línea aparecerá en su archivo" > prueba.txt

Esto no nos regresa ningún resultado pero podemos usar un comando más:

`cat`
-----

Imprime los contenidos de un Archivo

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    cat prueba.txt

    ## Esta línea aparecerá en su archivo

Y si abren el archivo desde Rstudio podrán verlo.

TL;DR Cli
---------

-   `pwd` Imprime el directorio en el que estás actualmente.
-   `ls` Comando que lista los archivos del directorio donde estás.
-   `mkdir` Make directory, crea un directorio.
-   `cd` Change directory, te mueve de directorio.
-   `touch` es un comando para crear un archivo vacío.
-   `echo` Comando para imprimir en pantalla lo que se le mande.
-   `>` este es un operador que le llamamos `pipe`.
-   `cat` Imprime los contenidos de un Archivo

Cheatsheet git
==============

Verificamos que estamos en la carpeta correcta

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    pwd 

    ## /home/skalas/github/aacd/sesion01/directorioPrueba

Generamos un nuevo repositorio (local) en nuestra carpeta de prueba

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    git init

    ## Initialized empty Git repository in /home/skalas/github/aacd/sesion01/directorioPrueba/.git/

Verificamos que esté funcional, y que tengamos un archivo sin commitear.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    git status

    ## On branch master
    ## 
    ## No commits yet
    ## 
    ## Untracked files:
    ##   (use "git add <file>..." to include in what will be committed)
    ##  prueba.txt
    ## 
    ## nothing added to commit but untracked files present (use "git add" to track)

Add
===

Agregamos un archivo para poderlo comitear después.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    git add prueba.txt
    git status

    ## On branch master
    ## 
    ## No commits yet
    ## 
    ## Changes to be committed:
    ##   (use "git rm --cached <file>..." to unstage)
    ##  new file:   prueba.txt

Commit
======

Hacemos el commit y esto hace que se quede guardado en la ‘historia’ de
git.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    git commit -m 'Prueba!!'

    ## [master (root-commit) 5a382e8] Prueba!!
    ##  1 file changed, 1 insertion(+)
    ##  create mode 100644 prueba.txt

Así se ve el mensaje cuando tienen el directorio limpio y sin cambios.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    git status

    ## On branch master
    ## nothing to commit, working tree clean

Diff
====

Nos muestra las diferencias entre la versión que tenemos en el commit y
la versión que hemos modificado.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    echo "nueva línea" > prueba.txt
    git diff

    ## diff --git a/prueba.txt b/prueba.txt
    ## index 9a88a77..24e6e86 100644
    ## --- a/prueba.txt
    ## +++ b/prueba.txt
    ## @@ -1 +1 @@
    ## -Esta línea aparecerá en su archivo
    ## +nueva línea

Si agregamos la bandera `--staged`, nos da las diferencias con lo que
esta en `staging`.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    git diff --staged

Acá se ve el diff después de hacer el stage.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    git add prueba.txt
    git diff --staged

    ## diff --git a/prueba.txt b/prueba.txt
    ## index 9a88a77..24e6e86 100644
    ## --- a/prueba.txt
    ## +++ b/prueba.txt
    ## @@ -1 +1 @@
    ## -Esta línea aparecerá en su archivo
    ## +nueva línea

Log
===

Nos muestra todos los commits que se han hecho en un repositorio.

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    echo "nueva línea1" > prueba.txt
    git add prueba.txt
    git commit -m 'Actualización prueba.txt'
    git log

    ## [master 42cac70] Actualización prueba.txt
    ##  1 file changed, 1 insertion(+), 1 deletion(-)
    ## commit 42cac70cff9c8c5b57de61df9275d9a6fe8e88ae
    ## Author: Miguel Escalante <miguel.escalante@itam.mx>
    ## Date:   Fri Apr 30 01:07:23 2021 -0500
    ## 
    ##     Actualización prueba.txt
    ## 
    ## commit 5a382e8b528988694bda5c1c828f7dfb3ba24a97
    ## Author: Miguel Escalante <miguel.escalante@itam.mx>
    ## Date:   Fri Apr 30 01:07:23 2021 -0500
    ## 
    ##     Prueba!!

Además podemos especificar un archivo particular para saber los commits
que han hecho modificaciones a determinado archivo

    cd directorioPrueba # Esto no lo corras, corre a partir de la siguiente línea
    echo "nueva línea1" > prueba2.txt
    git add prueba.txt
    git commit -m 'Nueva prueba'
    git log prueba2.txt

    ## On branch master
    ## Untracked files:
    ##   (use "git add <file>..." to include in what will be committed)
    ##  prueba2.txt
    ## 
    ## nothing added to commit but untracked files present (use "git add" to track)

    rm -rf directorioPrueba
